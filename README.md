# utils

#### 介绍
go开发常用工具方法，以重新封装第三方工具为主

#### 说明
| 包 | 包说明 |
| --- | --- |
| structure | 结构体相关工具方法 |
| uuid | uuid方法 |
| datetime | 日期相关工具方法 |
| logger | 日志工具 |


