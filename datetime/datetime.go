package datetime

import (
	"fmt"
	"time"
)

const (
	monthPattern  = "2006-01"
	datePattern   = monthPattern + "-02"
	timePattern   = "15:04:05"
	secondPattern = datePattern + " " + timePattern
	milliPattern  = secondPattern + ".999"
	microPattern  = milliPattern + "999"

	monthPatternClose  = "200601"
	datePatternClose   = monthPatternClose + "02"
	timePatternClose   = "150405"
	secondPatternClose = datePatternClose + " " + timePatternClose

	yearPatternCHS       = "2006年"
	monthPatternCHS      = yearPatternCHS + "01月"
	datePatternCHS       = monthPatternCHS + "02日"
	hourPatternCHS       = "15时"
	minutePatternCHS     = hourPatternCHS + "04分"
	timePatternCHS       = minutePatternCHS + "05秒"
	dateTimePatternCHS   = datePatternCHS + timePatternCHS
	dateHourPatternCHS   = datePatternCHS + hourPatternCHS
	dateMinutePatternCHS = datePatternCHS + minutePatternCHS
)

// ToMicro 2006-01-02 15:04:05.999999
func ToMicro(t time.Time) string {
	return t.Format(microPattern)
}

// ToMilli 2006-01-02 15:04:05.999
func ToMilli(t time.Time) string {
	return t.Format(milliPattern)
}

// ToTime 15:04:05
func ToTime(t time.Time) string {
	return t.Format(timePattern)
}

// ParseTime 15:04:05
func ParseTime(t string) (time.Time, error) {
	return time.Parse(timePattern, t)
}

// ToDateTime 2006-01-02 15:04:05
func ToDateTime(t time.Time) string {
	return t.Format(secondPattern)
}

// ParseDateTime 2006-01-02 15:04:05
func ParseDateTime(t string) (time.Time, error) {
	return time.Parse(secondPattern, t)
}

// ToMonth 2006-01
func ToMonth(t time.Time) string {
	return t.Format(monthPattern)
}

// ToDate 2006-01-02
func ToDate(t time.Time) string {
	return t.Format(datePattern)
}

// ParseDate 2006-01-02
func ParseDate(t string) (time.Time, error) {
	return time.Parse(datePattern, t)
}

// ParseDateTimeClose 20060102150405
func ParseDateTimeClose(t string) (time.Time, error) {
	return time.Parse(secondPatternClose, t)
}

// ToTimeClose 150405
func ToTimeClose(t time.Time) string {
	return t.Format(timePatternClose)
}

// ToDateTimeClose 20060102150405
func ToDateTimeClose(t time.Time) string {
	return t.Format(secondPatternClose)
}

// ToDateClose 20060102
func ToDateClose(t time.Time) string {
	return t.Format(datePatternClose)
}

// ToMonthClose 200601
func ToMonthClose(t time.Time) string {
	return t.Format(monthPatternClose)
}

// ParseDateClose 20060102
func ParseDateClose(t string) (time.Time, error) {
	return time.Parse(datePatternClose, t)
}

// GetDateClose 20060102
func GetDateClose() string {
	return time.Now().Format(datePatternClose)
}

// ToYearChs 2006年
func ToYearChs(t time.Time) string {
	return t.Format(yearPatternCHS)
}

// ToMonthChs 2006年01月
func ToMonthChs(t time.Time) string {
	return t.Format(monthPatternCHS)
}

// ToDateChs 2006年01月02日
func ToDateChs(t time.Time) string {
	return t.Format(datePatternCHS)
}

// ToDateHourChs 2006年01月02日15时
func ToDateHourChs(t time.Time) string {
	return t.Format(dateHourPatternCHS)
}

// ToDateMinChs 2006年01月02日15时04分
func ToDateMinChs(t time.Time) string {
	return t.Format(dateMinutePatternCHS)
}

// ToDateTimeChs 2006年01月02日15时04分05秒
func ToDateTimeChs(t time.Time) string {
	return t.Format(dateTimePatternCHS)
}

// ToMinChs 15时04分
func ToMinChs(t time.Time) string {
	return t.Format(minutePatternCHS)
}

// ToTimeChs 15时04分05秒
func ToTimeChs(t time.Time) string {
	return t.Format(timePatternCHS)
}

// GetMonth 2006-01
func GetMonth() string {
	return ToMonth(time.Now())
}

// GetDate 2006-01-02
func GetDate() string {
	return ToDate(time.Now())
}

// GetDateTime 2006-01-02 15:04:05
func GetDateTime() string {
	return ToDateTime(time.Now())
}

// GetDateTimeClose 20060102150405
func GetDateTimeClose() string {
	return ToDateTime(time.Now())
}

// GetTimestampNano 纳秒时间戳 1652854450879612000
func GetTimestampNano() string {
	return fmt.Sprint(time.Now().UnixNano())
}

// GetTimestampMicro 微妙时间戳 1652854450879582
func GetTimestampMicro() string {
	return fmt.Sprint(time.Now().UnixMicro())
}

// GetTimestampMilli 毫秒时间戳 1652854450879
func GetTimestampMilli() string {
	return fmt.Sprint(time.Now().UnixMilli())
}
