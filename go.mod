module gitee.com/gomod/utils

go 1.17

require (
	github.com/goinggo/mapstructure v0.0.0-20140717182941-194205d9b4a9
	github.com/google/uuid v1.3.0
	github.com/jinzhu/copier v0.3.5
)
