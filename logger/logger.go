package logger

import (
	"io"
	"log"
	"os"
	"strings"
)

var Log *log.Logger

var (
	Trace   *log.Logger // 记录所有日志
	Info    *log.Logger // 重要的信息
	Warning *log.Logger // 需要注意的信息
	Error   *log.Logger // 非常严重的问题
)

func init() {
	var mw io.Writer
	mw = os.Stdout

	flag := log.LstdFlags | log.Lmicroseconds | log.Lshortfile
	log.SetOutput(mw)
	log.SetFlags(flag)

	Trace = log.New(mw, "TRACE: ", flag)
	Info = log.New(mw, "INFO: ", flag)
	Warning = log.New(mw, "WARNING: ", flag)
	Error = log.New(mw, "\n\n\u001B[31mERROR: ", flag)
}

func InitLogger(file string) {
	if file != "" {
		var mw io.Writer
		createLogDir(file)
		f, e := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
		if e != nil {
			panic(e)
		}
		mw = io.MultiWriter(os.Stdout, f)
		log.SetOutput(mw)
		Trace.SetOutput(mw)
		Info.SetOutput(mw)
		Warning.SetOutput(mw)
		Error.SetOutput(mw)
	}
}

func createLogDir(file string)  {
	// 按照所需读写权限创建文件
	_, err := os.Stat(file)
	if err != nil {
		i := strings.LastIndex(file, "/")
		if i > -1 {
			dir := file[0: i]
			os.MkdirAll(dir, 0750)
		}
	}
}
