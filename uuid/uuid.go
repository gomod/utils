package uuid

import (
	"github.com/google/uuid"
	"strings"
)

// UUID UUID字符串
func UUID() string {
	return uuid.NewString()
}

// UUID32 去掉-的uuid字符串
func UUID32() string {
	return strings.ReplaceAll(UUID(), "-", "")
}
